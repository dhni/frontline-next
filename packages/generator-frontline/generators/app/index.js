'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const fse = require('fs-extra');
const slug = require('slugg');
const _union = require('lodash/union');

const FLAVOURS = {
    'vanilla': {
        requiredPackages: {
            '@dhni-foo/core': 'latest',
            '@dhni-foo/serviceworker': 'latest'
        },
        optionalPackages: {
            '@akqa-cph/utils': 'latest'
        }
    },
    'react': {
        requiredPackages: {
            'react': '^16.5.0',
            'react-dom': '^16.5.0',
            'prop-types': '^15.6.2',
            '@dhni-foo/core': 'latest',
            '@dhni-foo/serviceworker': 'latest'
        },
        optionalPackages: {
            '@dhni-foo/utils': 'latest'
        }
    }
};

module.exports = class extends Generator {

    constructor(args, opts) {
        super(args, opts);

        this.option('skip-welcome-message', {
            desc: 'Skipts the welcome message',
            type: Boolean
        });

        this.option('skip-install-message', {
            desc: 'Skips the message after the installation of dependencies',
            type: Boolean
        });

    }

    initializing() {}

    async prompting() {
        // Have Yeoman greet the user.
        this.log(
            yosay(`Welcome to the excellent ${chalk.red('generator-frontline')} generator!`)
        );

        const prompts = [
            // Project / Author details
            {
                type: 'input',
                name: 'projectName',
                validate: input => {
                    if(!input || input.length < 1) {
                        return 'Project name is required';
                    }

                    return true;
                },
                default: '$DEBUG$'
            },
            // TODO - get from git?
            {
                type: 'input',
                name: 'projectAuthor'
            },
            {
                type: 'input',
                name: 'projectAuthorEmail'
            },
            // Flavour
            {
                type: 'list',
                name: 'flavour',
                choices: this._getFlavoursForPrompts()
            },
            {
                type: 'checkbox',
                name: 'requiredPackages',
                choices: this._getRequiredPakcagesForPrompts('react'),
                when: answers => answers.flavour === 'react'
            },
            {
                type: 'checkbox',
                name: 'optionalPackages',
                choices: this._getOptionalPakcagesForPrompts('react'),
                when: answers => answers.flavour === 'react'
            },
            {
                type: 'checkbox',
                name: 'requiredPackages',
                choices: this._getRequiredPakcagesForPrompts('vanilla'),
                when: answers => answers.flavour === 'vanilla'
            },
            {
                type: 'checkbox',
                name: 'optionalPackages',
                choices: this._getOptionalPakcagesForPrompts('vanilla'),
                when: answers => answers.flavour === 'vanilla'
            },
            // CMS flavour ??
            {
                type: 'list',
                name: 'cmsFlavour',
                choices: ['sitecore', 'epi-server', 'umbraco']
            },
            // Templating Language ?
        ];

        this.props = await this.prompt(prompts);

        this.dependencies = this._getDependenciesFromAnswers(
            this.props.flavour,
            this.props.requiredPackages,
            this.props.optionalPackages
        );

    }

    writing() {

        const templates = [
            'package.json',
            'index.shtml',
            'offline.shtml',
            'static/src/js/main.js'
        ];
        templates.forEach(t => {
            this.fs.copyTpl(
                this.templatePath(t),
                this.destinationPath(t),
                this
            );
        });

        const asIs = [
            '.editorconfig',
            '.eslintrc.json',
            '.gitignore',
            '.stylelintrc.json',
            'gulpfile.js',
            'gulp',
            'setup'
        ];
        asIs.forEach(s => {
            try {
                fse.copySync(
                    this.templatePath(s),
                    this.destinationPath(s)
                );
            } catch (e) {
                throw e;
            }
        });

    }

    install() {
        this.npmInstall();
    }

    _getFlavoursForPrompts() {
        return Object.keys(FLAVOURS).map(key => ({
            name: key,
            value: key.toLowerCase(),
            short: key
        }));
    }

    _getRequiredPakcagesForPrompts(flavour) {
        return Object.keys(FLAVOURS[flavour].requiredPackages).map(key => ({
            name: key,
            value: key.toLowerCase(),
            short: key,
            checked: true
        }));
    }

    _getOptionalPakcagesForPrompts(flavour) {
        return Object.keys(FLAVOURS[flavour].optionalPackages).map(key =>  ({
            name: key,
            value: key.toLowerCase(),
            short: key,
            checked: false
        }));
    }

    _getDependenciesFromAnswers(flavour, requiredPackages = [], optionalPackages = []) {
        const deps = {};

        optionalPackages.forEach(k => {
            deps[k] = FLAVOURS[flavour].optionalPackages[k];
        });

        requiredPackages.forEach(k => {
            deps[k] = FLAVOURS[flavour].requiredPackages[k];
        });

        const orderedDeps = {};
        Object.keys(deps).sort().forEach(key => {
            orderedDeps[key] = deps[key];
        });

        return orderedDeps;
    }
};
