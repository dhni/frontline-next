﻿<!DOCTYPE html>
<html lang="da" class="no-js">
<head>
    <!-- !!! REMOVE THIS LINE WHEN GOING LIVE !!! -->
    <meta name="robots" content="noindex, nofollow">

    <meta charset="utf-8">
    <!-- Add to web.config -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Frontline</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">

    <!--
        Create a block of inline styling to display until the primary CSS is ready. You should use whatever
        automatic inlining-capabilities your CMS provides:
    -->
    <style type="text/css"><!--#include virtual="/static/dist/css/critical.min.css" --></style>
    <link rel="preload" href="/static/dist/css/main.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" type="text/css" href="/static/dist/css/main.min.css"></noscript>

    <!-- Preload important stuff like your fonts, too. Remember "crossorigin", even if they're self-hosted!
        <link rel="preload" href="/static/dist/fonts/some-font.woff2" as="font" type="font/woff2" crossorigin="anonymous">
        <link rel="preload" href="/static/dist/fonts/some-font.ttf"   as="font" type="font/ttf"   crossorigin="anonymous">
    -->

    <!-- You can use "prefetch" to preload other important assets (if you're not using HTTP/2 push) -->
    <link rel="prefetch" href="/static/dist/js/main.min.js">

    <!-- And you can tell the browser to pre-connect to other sites you know you are going to talk to eventually: -->
    <link rel="dns-prefetch" href="https://cdn.polyfill.io">

    <script type="text/javascript"><!--#include virtual="/static/dist/js/modernizr.js" --></script>
</head>
<body>

<div data-react-element="Foo">
    <script>
        {
            "message": "John Fatsvag"
        }
    </script>
    <div>
        <p>
            Lollele
        </p>
    </div>
</div>

<header>
    <h1>frontline</h1>
</header>

<main>

    <p>This is the baseline frontline. It contains very little styling and code, so it should be easy to expand
        upon. Have fun!</p>

    <hr>

    <section>

        <h1>Bootstrapper demo</h1>

        <p>
            Each block here is controlled by an instance of the example JS module, started by the JS bootstrapper.
        </p>


        <style>
            /* Demo styling for the purpose of the example modules. */
            article[data-module=example] {
                border: 1px solid #aaa;
                background: rgba(0,0,0, .05);
                padding: 20px;
                margin: 20px;
            }

            article[data-module=example] small {
                display: none;
            }

            article[data-module=example].example--initialized {
                background: rgba(0,255,0, .1);
            }

            article[data-module=example].example--initialized > small {
                display: block;
            }

            article[data-module=example].example--testing {
                font-style: italic;
            }
        </style>

        <article data-module="example">
            <h2>Example module</h2>
            <small>If you can read this, the module has been bootstrapped and is ready.</small>
            <p>
                <button type="button" class="demobutton">Click me!</button>
                <button type="button" class="killbutton">Kill instance</button>
            </p>
        </article>
        <article data-module="example">
            <h2>Example module</h2>
            <small>If you can read this, the module has been bootstrapped and is ready.</small>
            <p>
                <button type="button" class="demobutton">Click me!</button>
                <button type="button" class="killbutton">Kill instance</button>
            </p>

            <article data-module="example">
                <h2>Nested example module</h2>
                <small>If you can read this, the module has been bootstrapped and is ready.</small>
                <p>
                    <button type="button" class="demobutton">Click me!</button>
                    <button type="button" class="killbutton">Kill instance</button>
                </p>
            </article>
        </article>
    </section>

    <hr>

    <section>
        <h1>Image handler demo</h1>
        <div class="imagehandler"
             data-src-xxs="http://placehold.it/480x250/ffaa00/ffffff/?text=xxs"
             data-src-xs="http://placehold.it/480x250/ffaa00/ffffff/?text=xs"
             data-src-sm="http://placehold.it/768x250/ffaa00/ffffff/?text=sm"
             data-src-md="http://placehold.it/1280x250/ffaa00/ffffff/?text=md"
             data-src-lg="http://placehold.it/1280x250/ffaa00/ffffff/?text=lg"
             data-src-xl="http://placehold.it/1600x250/ffaa00/ffffff/?text=xl"
             data-src-xxl="http://placehold.it/1920x250/ffaa00/ffffff/?text=xxl"
             data-alt="Dum Alt text">
            <noscript>
                <img src="http://placehold.it/480x250/ffaa00/ffffff/?text=NoScriptImage" alt="Dum Alt text">
            </noscript>
        </div>
    </section>

    <hr>

    <section>

        <h1>Grid</h1>

        <style>
            /* A bit of demo-styling to make the grid easier to see. */
            .col pre {
                background: #eee;
                overflow: hidden;
                margin: 0;
                outline: 1px solid #ddd;
            }
        </style>

        <div class="grid grid-container">
            <div class="row">
                <div class="col">
                    <pre>col</pre>
                </div>
            </div>
            <div class="row row--center">
                <div class="col col--xxs-6 col--xs-4 col--sm-2 col--md-1">
                <pre>col--xxs-6
col--xs-4
col--sm-2
col--md-1</pre>
                </div>
                <div class="col col--xxs-6 col--xs-4 col--sm-2 col--md-1">
                <pre>col--xxs-6
col--xs-4
col--sm-2
col--md-1</pre>
                </div>
                <div class="col col--xxs-6 col--xs-4 col--sm-2 col--md-1">
                <pre>col--xxs-6
col--xs-4
col--sm-2
col--md-1</pre>
                </div>
                <div class="col col--xxs-6 col--xs-4 col--sm-2 col--md-1">
                <pre>col--xxs-6
col--xs-4
col--sm-2
col--md-1</pre>
                </div>
                <div class="col col--xxs-6 col--xs-4 col--sm-2 col--md-1">
                <pre>col--xxs-6
col--xs-4
col--sm-2
col--md-1</pre>
                </div>
                <div class="col col--xxs-6 col--xs-4 col--sm-2 col--md-1">
                <pre>col--xxs-6
col--xs-4
col--sm-2
col--md-1</pre>
                </div>
                <div class="col col--xxs-6 col--xs-4 col--sm-2 col--md-1">
                <pre>col--xxs-6
col--xs-4
col--sm-2
col--md-1</pre>
                </div>
                <div class="col col--xxs-6 col--xs-4 col--sm-2 col--md-1">
                <pre>col--xxs-6
col--xs-4
col--sm-2
col--md-1</pre>
                </div>
                <div class="col col--xxs-6 col--xs-4 col--sm-2 col--md-1">
                <pre>col--xxs-6
col--xs-4
col--sm-2
col--md-1</pre>
                </div>
                <div class="col col--xxs-6 col--xs-4 col--sm-2 col--md-1">
                <pre>col--xxs-6
col--xs-4
col--sm-2
col--md-1</pre>
                </div>
                <div class="col col--xxs-6 col--xs-4 col--sm-2 col--md-1">
                <pre>col--xxs-6
col--xs-4
col--sm-2
col--md-1</pre>
                </div>
                <div class="col col--xxs-6 col--xs-4 col--sm-2 col--md-1">
                <pre>col--xxs-6
col--xs-4
col--sm-2
col--md-1</pre>
                </div>
                <div class="col col--md-2">
                    <pre>col--md-2</pre>
                </div>
                <div class="col col--md-2">
                    <pre>col--md-2</pre>
                </div>
                <div class="col col--md-2">
                    <pre>col--md-2</pre>
                </div>
                <div class="col col--md-2">
                    <pre>col--md-2</pre>
                </div>
                <div class="col col--md-2">
                    <pre>col--md-2</pre>
                </div>
                <div class="col col--md-2">
                    <pre>col--md-2</pre>
                </div>
                <div class="col col--md-3">
                    <pre>col--md-3</pre>
                </div>
                <div class="col col--md-3">
                    <pre>col--md-3</pre>
                </div>
                <div class="col col--md-3">
                    <pre>col--md-3</pre>
                </div>
                <div class="col col--md-3">
                    <pre>col--md-3</pre>
                </div>
                <div class="col col--md-4">
                    <pre>col--md-4</pre>
                </div>
                <div class="col col--md-4">
                    <pre>col--md-4</pre>
                </div>
                <div class="col col--md-4">
                    <pre>col--md-4</pre>
                </div>
                <div class="col col--md-6">
                    <pre>col--md-6</pre>
                </div>
                <div class="col col--md-6">
                    <pre>col--md-6</pre>
                </div>
                <div class="col col--md-12">
                    <pre>col--md-12</pre>
                </div>
            </div>
        </div>
    </section>

    <hr>

    <section>

        <h1>Inlining SVGs and scripts:</h1>

        <ul>
            <li>
                <b>Cross-platform C# CMS (confirmed on SiteCore, Umbraco, EpiServer):</b><br/>
                <code>@Html.Raw(File.ReadAllText(Server.MapPath("~/static/dist/svg/name-of-svg.svg")))</code><br/><br/>
            </li>
            <li>
                <b>Umbraco (newer baseline):</b><br/>
                <code>@Static.IncludeFile("~/static/dist/svg/name-of-svg.svg")</code><br/><br/>
            </li>
            <li>
                <b>SHTML:</b><br/>
                <code>&lt;!--#include virtual="/static/dist/svg/name-of-svg.svg" --></code><br/><br/>
            </li>
            <li>
                <b>Javascript:</b><br/>
                <code>import someIcon from '../svg-individual/name-of-svg.svg'; window.console.log(someIcon);</code><br/><br/>
            </li>
        </ul>
    </section>

</main>



<script>
    var scriptLoaderSettings = {
        mainScriptSrc: [
            '/static/dist/js/main.min.js'
        ],
        polyfills: 'default-3.6'
    };

    var loadMainScript=function(){function a(a){var b=document.createElement("script");b.defer=!0,b.async=!!0,b.src=a,document.body.appendChild(b)}if("object"==typeof scriptLoaderSettings.mainScriptSrc)for(var b=0;b<scriptLoaderSettings.mainScriptSrc.length;b++)a(scriptLoaderSettings.mainScriptSrc[b]);else a(scriptLoaderSettings.mainScriptSrc)};
    !function(a,b){var c,d=[/^(.+)(\[.+MessengerForiOS.+\][ ]*)/,/^(.+)(\[.+FBIOS.+\][ ]*)/,/^(.+)( Twitter for iPhone)/];for(c=0;c<d.length;++c){var e=d[c],f=a.match(e);if(f){b=b+"&ua="+encodeURIComponent(f[1]+" Safari");break}}var g=document.createElement("script");g.src=b,document.body.appendChild(g)}(navigator.userAgent,"https://cdn.polyfill.io/v2/polyfill.min.js?features="+scriptLoaderSettings.polyfills+"&callback=loadMainScript");

    /*! loadCSS rel=preload polyfill. [c]2017 Filament Group, Inc. MIT License - https://github.com/filamentgroup/loadCSS */
    !function(t){"use strict";t.loadCSS||(t.loadCSS=function(){});var e=loadCSS.relpreload={};if(e.support=function(){var e;try{e=t.document.createElement("link").relList.supports("preload")}catch(t){e=!1}return function(){return e}}(),e.bindMediaToggle=function(t){function e(){t.media=a}var a=t.media||"all";t.addEventListener?t.addEventListener("load",e):t.attachEvent&&t.attachEvent("onload",e),setTimeout(function(){t.rel="stylesheet",t.media="only x"}),setTimeout(e,3e3)},e.poly=function(){if(!e.support())for(var a=t.document.getElementsByTagName("link"),n=0;n<a.length;n++){var o=a[n];"preload"!==o.rel||"style"!==o.getAttribute("as")||o.getAttribute("data-loadcss")||(o.setAttribute("data-loadcss",!0),e.bindMediaToggle(o))}},!e.support()){e.poly();var a=t.setInterval(e.poly,500);t.addEventListener?t.addEventListener("load",function(){e.poly(),t.clearInterval(a)}):t.attachEvent&&t.attachEvent("onload",function(){e.poly(),t.clearInterval(a)})}"undefined"!=typeof exports?exports.loadCSS=loadCSS:t.loadCSS=loadCSS}("undefined"!=typeof global?global:this);

    // Enable this to register a service worker. Please make sure you understand the implications of doing so.
    // if ("serviceWorker" in navigator) { navigator.serviceWorker.register("/sw.js", { scope: '/' }); }
</script>
</body>
</html>
