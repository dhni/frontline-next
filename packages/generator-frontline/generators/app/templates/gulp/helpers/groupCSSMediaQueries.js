/**
 * This file is a "hacked" version, combining:
 *  - gulp-group-css-media-queries ( http://npmjs.com/package/gulp-group-css-media-queries )
 *  - group-css-media-queries ( https://www.npmjs.com/package/group-css-media-queries )
 *  - group-css-media-queries PR #16 ( https://github.com/SE7ENSKY/group-css-media-queries/pull/16 )
 *
 * A clean NPM package would've been nicer, but since development on the original package seems to have
 * stalled somewhat, we're gonna build our own utility instead. With black jack! And hookers!
 */


const through = require('through2');
const parseCss = require('css-parse');
const stringifyCss = require('css-stringify');
const indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };


function groupMediaQueries(css) {
    var emToPxRatio, i, intervalRules, len, m, media, mediaRules, medias, onlyMaxRules, onlyMinRules, otherRules, parsed, ref, rootRules, rule, rules;
    parsed = parseCss(css);
    medias = {};
    rootRules = [];
    ref = parsed.stylesheet.rules;
    for (i = 0, len = ref.length; i < len; i++) {
        rule = ref[i];
        if (rule.type === 'media') {
            if (!medias[rule.media]) {
                medias[rule.media] = [];
            }
            medias[rule.media] = medias[rule.media].concat(rule.rules);
        } else {
            rootRules.push(rule);
        }
    }
    mediaRules = [];
    for (media in medias) {
        rules = medias[media];
        rule = {
            type: "media",
            media: media,
            rules: rules
        };
        if (media.indexOf("min-width") !== -1) {
            m = media.match(/min-width:\s*(\d+(?:\.\d*)?)(px|r?em)?/);
            if (m && m[1]) {
                rule.minWidth = parseFloat(m[1]);
            }
            if (m[2]) {
                rule.unit = m[2];
            }
        }
        if (media.indexOf("max-width") !== -1) {
            m = media.match(/max-width:\s*(\d+(?:\.\d*)?)(px|r?em)?/);
            if (m && m[1]) {
                rule.maxWidth = parseFloat(m[1]);
            }
            if (m[2]) {
                rule.unit = m[2];
            }
        }
        mediaRules.push(rule);
    }
    onlyMinRules = mediaRules.filter(function(rule) {
        return (rule.minWidth != null) && (rule.maxWidth == null);
    });
    onlyMaxRules = mediaRules.filter(function(rule) {
        return (rule.maxWidth != null) && (rule.minWidth == null);
    });
    intervalRules = mediaRules.filter(function(rule) {
        return (rule.minWidth != null) && (rule.maxWidth != null);
    });
    otherRules = mediaRules.filter(function(rule) {
        return indexOf.call(onlyMinRules.concat(onlyMaxRules).concat(intervalRules), rule) < 0;
    });
    emToPxRatio = 16;
    onlyMinRules.sort(function(a, b) {
        var aPxValue, bPxValue;
        aPxValue = a.minWidth;
        bPxValue = b.minWidth;
        if (a.unit === 'em' || a.unit === 'rem') {
            aPxValue *= emToPxRatio;
        }
        if (b.unit === 'em' || b.unit === 'rem') {
            bPxValue *= emToPxRatio;
        }
        return aPxValue - bPxValue;
    });
    onlyMaxRules.sort(function(a, b) {
        var aPxValue, bPxValue;
        aPxValue = a.maxWidth;
        bPxValue = b.maxWidth;
        if (a.unit === 'em' || a.unit === 'rem') {
            aPxValue *= emToPxRatio;
        }
        if (b.unit === 'em' || b.unit === 'rem') {
            bPxValue *= emToPxRatio;
        }
        return bPxValue - aPxValue;
    });
    parsed.stylesheet.rules = rootRules.concat(onlyMinRules).concat(onlyMaxRules).concat(intervalRules).concat(otherRules);
    return stringifyCss(parsed);
}


module.exports = function () {
    return through.obj(function (file, enc, cb) {
        if (file.isNull()) {
            this.push(file);
            return cb();
        }

        try {
            file.contents = new Buffer(groupMediaQueries(file.contents.toString()));
        } catch (err) {
            this.emit('error', err.message);
        }

        this.push(file);
        cb();
    });
};

