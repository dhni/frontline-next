/*global __dirname */

// This is a copy of "browsersync-ssi", modified to use "node-ssi" instead of plain old "ssi".
// Not that "node-ssi" is perfect, but "ssi" was riddled with errors and tried to inject a wrong version
// of browsersync causing 404 errors. This copy also has a bugfix to make it work with URLs with queries.
// https://github.com/soenkekluth/browsersync-ssi
module.exports = function browserSyncSSI(options) {
    const SSI = require('node-ssi');
    const path = require('path');
    const fs = require('graceful-fs');

    const opt = options || {};
    const ext = opt.ext || '.shtml';
    const baseDir = opt.baseDir || __dirname;

    const parser = new SSI({
        baseDir,
        encoding: 'utf-8'
    });

    return (req, res, next) => {

        // Split the URL to get the path name and remove the query, so we can check the file name
        const url = req.originalUrl || req.url;
        const urlSplit = url.split('?');
        const pathName = urlSplit[0];
        const filename = path.join(baseDir, pathName.substr(-1) === '/' ? `${pathName}index${ext}` : pathName);

        // Parse the file if it has the correct extension
        if (filename.toLowerCase().indexOf(ext.toLowerCase()) > -1 && fs.existsSync(filename)) {

            // Get content of file
            const originalContents = fs.readFileSync(filename, {
                encoding: 'utf8'
            });

            // Parse the file and send it to browsersync
            parser.compile(originalContents, (err, contents) => {
                res.writeHead(200, {
                    'Content-Type': 'text/html'
                });
                res.end(contents);
            });

        }

        // The file didn't have the correct extension and will be ignored by this middleware
        else {
            next();
        }

    };
};
