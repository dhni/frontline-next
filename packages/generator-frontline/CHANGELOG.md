# Changelog
All notable changes to **Frontline** will be documented in this file.  
Versioning adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


***


## [3.6.2](#markdown-header-362-2018-09-14) 2018-09-14

### Changed
- `js/utils/dom/tabDomElements.js` has been updated. You can now give it an array of elements to exclude.



## [3.6.1](#markdown-header-361-2018-09-14) 2018-09-14

### Added
- Some customized SSI middleware for BrowserSync at `gulp/tasks/helpers/browsersync-ssi.js`.
- A new depencency `node-ssi`. 
- A `CHANGELOG.md` file.

### Changed
- `gulp/tasks/helpers/browsersync.js` is using our own `browsersync-ssi.js` instead of the dependency `browsersync-ssi`.
- From now on, we'll bump the version number (adhering to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)) in `package.json` every time there are any changes.

### Removed
- The `browsersync-ssi` dependency.
