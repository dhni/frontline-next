/**
 * Utilities for checking properties and states of elements.
 *
 * @module utils/dom/elementProperties
 * @author Lars Munkholm <lars.munkholm@akqa.com>
 */


/**
 * Check if an element is empty.
 *
 * @param {Element} element - Check if this element is empty.
 * @param {boolean} [strict=true] - Set this to **false** to ignore nodes with whitespace.
 * @returns {boolean} **True** if the element is empty.
 */
export function elementIsEmpty(element, strict = true) {
    return strict ? !element.childNodes.length : !element.innerHTML.trim().length;
}


/**
 * Check if an element is hidden in the DOM with `display: none;`
 *
 * @param {Element} element - The element to check.
 * @returns {boolean} **True** if element is hidden, otherwise **false**.
 */
export function elementIsHidden(element) {
    return element.offsetParent === null;
}
