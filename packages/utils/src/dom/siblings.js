/**
 * Utility to get **all the siblings** for the given DOM-element, or a subset thereof.
 *
 * @module utils/dom/siblings
 * @author Lars Munkholm <lars.munkholm@akqa.com>
 */


import {forEach} from "../forEach";

/**
 * Get all siblings of element, or a subset thereof.
 *
 * @param {Element} element - The target element.
 * @param {Boolean} [includeOriginalElement=false] - Set to true to include the original element among the siblings
 * @param {Element|Null} [fromElement=null] - Return the siblings starting at this element
 * @param {Element|Null} [untilElement=null] - Return the siblings stopping at this element
 * @returns {Element[]} Array of elements that are siblings to the given element.
 */
export function siblings(element, includeOriginalElement = false, fromElement = null, untilElement = null) {

    if (includeOriginalElement && !fromElement && !untilElement) {

        // Return array including the original element and all its siblings
        return Array.from(element.parentNode.children);

    } else {

        const siblings = [];

        // Set the element to start looking for siblings from
        let nextElement = fromElement || element.parentNode.firstElementChild;


        do {

            const currentElement = nextElement;
            const sameAsOriginalElement = element === currentElement;

            // Set next element to look at
            nextElement = nextElement.nextElementSibling;

            // Add this element to the list of sibling
            // unless it is the same as the original element (and this should be left out)
            if (!sameAsOriginalElement || includeOriginalElement) {
                siblings.push(currentElement);
            }

            // Stop looking for siblings, if the loop is set to stop at the current element
            if (currentElement === untilElement) {
                break;
            }

        } while (nextElement);


        // Return array of elements
        return siblings;

    }

}


/**
 * Get all the siblings **after** the given element.
 *
 * @param {Element} element - The target element.
 * @param {Boolean} [includeOriginalElement=false] - Set to true to include the original element among the siblings
 * @returns {Element[]} An array containing the elements following the given element (and possibly the element itself).
 */
export function nextSiblings(element, includeOriginalElement = false) {
    return siblings(element, includeOriginalElement, element);
}


/**
 * Get previous siblings of element
 *
 * @param {Element} element - The target element.
 * @param {Boolean} [includeOriginalElement=false] - Set to true to include the original element among the siblings
 * @returns {Element[]} An array containing the elements preceding the given element (and possibly the element itself).
 */
export function previousSiblings(element, includeOriginalElement = false) {
    return siblings(element, includeOriginalElement, null, element);
}


/**
 * Check if two elements are siblings.
 *
 * @param {Element} element1 - Check if this is a sibling to element2.
 * @param {Element} element2 - Check if this is a sibling to element1.
 * @param {boolean} [adjacentOnly=false] - Set this to **true** to only looks for adjacent siblings (meaning just before or after).
 * @returns {boolean} **True** if the element is a parent to the other element.
 */
export function elementsAreSiblings(element1, element2, adjacentOnly = false) {
    if (adjacentOnly) {

        return (element1.nextElementSibling === element2 || element1.previousElementSibling === element2);

    } else {

        // We'll start off with assuming that the elements aren't siblings, since most aren't
        let siblingsCheck = false;

        // Get siblings of element1
        const siblingElements = siblings(element1);

        // Loop though siblings and check if any of them are element2
        forEach(siblingElements, sibling => sibling === element2 ? siblingsCheck = true : null);

        // If element2 wasn't found amongst element1's children, return false
        return siblingsCheck;

    }
}
