/**
 * Helper-utilities for inserting, moving and emptying DOM-elements.
 *
 * @module utils/dom/elementManipulation
 * @author Lars Munkholm <lars.munkholm@akqa.com>
 */


import {siblings} from "./siblings";
import {forEach} from "../forEach";


/**
 * Delete an element from the DOM.
 * @param {Element|Element[]|Node|NodeList} element - Element(s) to delete.
 */
export function deleteElement(element) {
    forEach(element, currentElement =>
        currentElement.parentNode ?
            currentElement.parentNode.removeChild(currentElement) :
            null
    );
}


/**
 * **Append an element** (or multiple) inside another element, which means it will be placed as the last child.
 *
 * @param {Element|Element[]|Node|NodeList} element - The element(s) to append.
 * @param {Element} container - Append the element inside this container.
 */
export function appendElement(element, container) {
    forEach(element, currentElement => container.appendChild(currentElement));
}


/**
 * **Prepend an element** (or multiple) inside another element, which means it will be placed as the first child.
 *
 * @param {Element|Element[]|Node|NodeList} element - The element(s) to prepend.
 * @param {Element} container - Prepend the element inside this container.
 */
export function prependElement(element, container) {
    let lastInsertedElement;

    forEach(element, currentElement => {
        if (lastInsertedElement) {
            insertElementAfter(currentElement, lastInsertedElement);
        } else if (container.firstChild) {
            insertElementBefore(currentElement, container.firstChild);
        } else {
            appendElement(currentElement, container);
        }

        lastInsertedElement = currentElement;
    });
}


/**
 * Insert one or more elements **after** another element.
 *
 * @param {Element|Element[]|Node|NodeList} newElement - The element(s) to insert after another.
 * @param {Element|Node} existingElement - The existing element to insert after.
 */
export function insertElementAfter(newElement, existingElement) {
    const parent = existingElement.parentNode;
    let lastInsertedElement;

    if (parent) {
        forEach(newElement, currentElement => {
            if (lastInsertedElement) {
                parent.insertBefore(currentElement, lastInsertedElement.nextSibling);
            } else {
                parent.insertBefore(currentElement, existingElement.nextSibling);
            }

            lastInsertedElement = currentElement;
        });
    }
}


/**
 * Insert one or more elements **before** another element.
 *
 * @param {Element|Element[]|Node|NodeList} newElement - The element(s) to insert before another.
 * @param {Element|Node} existingElement - The existing element to insert before.
 */
export function insertElementBefore(newElement, existingElement) {
    forEach(newElement, currentElement => existingElement.parentNode.insertBefore(currentElement, existingElement));
}


/**
 * **Swap two elements** with each other.
 *
 * @param {Element} element1 - This will be replaced with `element2`
 * @param {Element} element2 - This will be replaced with `element1`
 */
export function swapElements(element1, element2) {

    // Remember the location of element2
    const parent2 = element2.parentNode;
    const next2 = element2.nextSibling;

    // If element1 is the next sibling of element2
    if (next2 === element1) {
        parent2.insertBefore(element1, element2);
    }

    // Otherwise, insert element2 right before element1
    else {
        element1.parentNode.insertBefore(element2, element1);

        // And now insert element1 where element2 was
        if (next2) {
            // If there was an element after element2, then insert element1 right before that
            parent2.insertBefore(element1, next2);
        } else {
            // Otherwise, just append as last child
            parent2.appendChild(element1);
        }
    }
}


/**
 * **Replace an element** with another one.
 *
 * @param {Element} oldElement - The existing element to be replaced.
 * @param {Element} newElement - The new element to take its place.
 */
export function replaceElement(oldElement, newElement) {
    insertElementAfter(newElement, oldElement);
    oldElement.parentElement.removeChild(oldElement);
}


/**
 * **Wrap an element** in a new element.
 *
 * @param {Element} existingElement - The existing element, which is about to be wrapped.
 * @param {Element} newWrapper - The new element which the existing one should be wrapped inside.
 *
 * @example
 * import { wrapElement } from "./utils/dom/elementManipulation";
 * import { createElement } from "./utils/dom/createElement";
 *
 * const newWrapper = createElement("div", {className: "wrapper"});
 * const wrapThis = document.getElementById("wrapThis");
 *
 * wrapElement(wrapThis, newWrapper);
 */
export function wrapElement(existingElement, newWrapper) {
    existingElement.parentNode.insertBefore(newWrapper, existingElement);
    newWrapper.appendChild(existingElement);
}


/**
 * **Inner wrap an element** in a new element.
 *
 * Actually, the children are what will be wrapped.
 *
 * @param {Element} existingElement - The existing element, which is about have its children wrapped.
 * @param {Element} newWrapper - The new element which the existing one should be wrapped inside.
 */
export function wrapInnerElement(existingElement, newWrapper) {
    while (existingElement.childNodes.length > 0) {
        newWrapper.appendChild(existingElement.childNodes[0]);
    }

    appendElement(newWrapper, existingElement);
}


/**
 * **Unwrap an element** by moving said element out on its parent and deleting the parent.
 *
 * @param {Element} element - The element to unwrap.
 * @param {boolean} [keepSiblings=true] - The element to unwrap.
 *
 * @example
 * import { unwrapElement } from "./utils/dom/elementManipulation";
 * const unwrapThis = document.getElementById("unwrapThis");
 * wrapElement(unwrapThis);
 * // The element just lost its parent and now lives with its grandparent.
 */
export function unwrapElement(element, keepSiblings = true) {
    const parent = element.parentElement;

    if (keepSiblings) {

        const allSiblings = siblings(element, true);
        forEach(allSiblings, sibling => insertElementBefore(sibling, parent));
        deleteElement(parent);

    } else {

        replaceElement(parent, element);

    }
}


/**
 * Empty one or more elements by removing all children.
 *
 * This is more than 75% faster than `innerHTML = ""` according to performance tests on jsPerf.com.
 *
 * @param {Element|Element[]|Node|NodeList} element - The element(s) to be emptied.
 */
export function emptyElement(element) {
    forEach(element, currentElement => {
        while (currentElement.firstChild) {
            currentElement.removeChild(currentElement.firstChild);
        }
    });
}
