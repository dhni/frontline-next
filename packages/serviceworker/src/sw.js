import './utils/serviceWorkerEssentials';
import createSVG from './utils/createSVG';
import cachingStrategies from './utils/cache/cachingStrategies.json';
import { setOfflinePage, setOfflineImage } from './utils/cache/offlineFallback';
import { setSkipWaiting } from './utils/events/install';
import { setClaimClients } from './utils/events/activate';
import { onMessage } from './utils/events/message';
import { cacheItems, precacheItems } from './utils/cache';
import { addRoute, ignoreRoutes, setMaxCachedFetches } from './utils/routes';
import { redirect } from './utils/redirect';


/**
 * Routes, ignoreRoutes and redirect use regular expressions (RegExp).
 * These are the basics:
 *
 * ^ = starts with
 * $ = ends with
 * . = any single character
 * * = find zero or more of the previous characters ("Ab*c" finds "Ac", "Abc", "Abbc", "Abbbc", and so on)
 * + = find one or more of the previous characters ("AX.+4" finds "AXx4", but not "AX4")
 * ? = find zero or one of previous characters ("Texts?" finds "Text" and "Texts" and "x(ab|c)?y" finds "xy", "xaby", or "xcy")
 * \ = escapes a character ("example.com\?search=string\+with\+plusses")
 *
 *
 * When adding routes or using cacheItems() or precacheItems() you should provide some settings:
 *
 * Make sure to set a fitting strategy (cacheFirst, cacheNetworkRace, cacheOnly, networkFirst, networkOnly)
 * "eventuallyFresh" updates the cached file from the network, after returning the cached version.
 * "expirationDate" stops caching files after a specified date (can be a timestamp or a Date object).
 * "maxAge" is the maximum age in milliseconds. If a cached item is older, it will be updated.
 * "hardCache" saves items in a cache that doesn't update when the Service Worker is updated.
 * "noCors" should only be uses when caching external items that don't support CORS (or when you're on localhost)
 */


// Show console logs, making it easier to debug.
// The placeholder ##INSERT-LOGGING## (with the comment indicators in front!)
// will be replaced with "enableLog();" from "./utils/log" automatically.
// This is disabled when building for production! The magic happens in the "serviceworkers" task,
// handled by WebPack's StringReplacePlugin.
// ##INSERT-LOGGING##


// Make this Service Worker active immediately (and update from existing SW version)
// and make it take control of all clients.
// Up until now, files have been fetched from the server, but from now on this SW
// will function as a proxy - if these are set to true.
setSkipWaiting(true);
setClaimClients(true);

// Set max amount of fetches to cache.
// These are the files that matches the routes defined later on,
// not files cached with cacheItems() and precacheItems()
setMaxCachedFetches(32);

// Set offline fallback page.
// This is shown instead of all pages that aren't cached, if the user is offline.
// This page is automatically cached with precacheItems() so you don't have to.
setOfflinePage('/offline.shtml');

// Set offline fallback image (instead of caching an image, let's create an SVG).
// This is shown instead of any image that isn't cached, if the user is offline .
setOfflineImage(createSVG('offline'));


// Add files to precache.
// These files are cached during the installation of the Service Worker,
// and are therefore treated as dependencies.
// The Service Worker won't complete it's installation until these are downloaded.
precacheItems([
    '/static/dist/js/main.min.js',
    '/static/dist/css/main.min.css'
], {
    strategy: cachingStrategies.cacheOnly,
    eventuallyFresh: true // Always update these files after returning the one from the cache
});


// Cache files after installation is done.
// ---- IMPORTANT ----
// Keep in mind that cached HTML-pages and Browser Sync don't play nice together.
// Cached HTML-pages won't be refreshed automatically, when changes occur.
// -------------------
cacheItems('/', {
    hardCache: true, // Save item in a cache that doesn't update when the Service Worker is updated
    strategy: cachingStrategies.networkFirst,
    maxAge: 2 * 24 * 3600000, // 2 days (1 hour = 3600000 ms) - update cached file after 2 days
    expirationDate: new Date('2019-12-19') // Remove file from cache if it's expired
});


// Add routes.
// All files that matches these regular expressions will be handles by the Service Worker.
addRoute([
    /\.map$/
], {
    strategy: cachingStrategies.cacheFirst,
    eventuallyFresh: true,
    maxAge: 2 * 24 * 3600000, // 2 days (1 hour = 3600000 ms) - update cached file after 2 days
});

addRoute([
    /polyfill\.io\//
], {
    hardCache: true,
    strategy: cachingStrategies.cacheFirst,
    noCors: true // Use only when caching external items (that don't support CORS or when you're on localhost)
});


// Redirect URLs matching a RegExp pattern to another file.
redirect(/\/static\/dist\/js\/main\.min\.old\.js/, '/static/dist/js/main.min.js');


// Ignore routes.
// If requested URLs match any of these regular expressions, they will bypass the Service Worker.
// Such files won't be cached.
ignoreRoutes([
    /\/admin/,
    /\/sitecore/,
    /\/umbraco/,
    /\/browser-sync/
]);


// You can listen for messages from the client browser.
// These messages have a command, and possibly an object with data.
// This command adds the specified files to the cache.
onMessage('add to cache', messageData => {
    cacheItems(messageData.files, messageData.settings);
});
